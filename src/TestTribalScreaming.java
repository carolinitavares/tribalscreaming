import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestTribalScreaming {

	
	/*
	 * Activity #4 Tasks - Tribal Screaming at 1 Person 
	 * 1. One person is amazing
	 * 2. Nobody is listening
	 * 3. Peter is shouting
	 * 
	 * Activity #5 Tasks - Tribal Screaming at More than 1 Person
	 * 4. Two (2) people are amazing
	 * 5. More than 2 people are amazing
	 * 6. Shouting at a lot of people
	 */
	
	/* R1: REFACTOR - function modified to get better, clean and functional code
	 * 
	 * Write a function that says 1 person is amazing.
	 * Function stub: scream(name)
	 * Example:  If name is “Peter”, the function should return this string: “Peter is amazing”
	 /
	@Test
	void testOnePersonIsAmazing() {
		TribalScreaming ts = new TribalScreaming();
		String result = ts.scream("Peter");
		assertEquals("Peter is amazing", result);
	}

	/* R2: REFACTOR - function updated to not change parameters values - TC PASS
	 * 
	 * Example: Modify your function so that when name is null, the function will return: “You is amazing”
	 /
	@Test
	void testNobodyIsListening() {
		TribalScreaming ts = new TribalScreaming();
		String result = ts.scream(null);
		assertEquals("You is amazing", result);
	}
	
	/* R3: REFACTOR - made some changes inside function to pass all the TCs and have a clean code - TC PASS
	 * result should be all uppercased
	 * 
	 * When name is all uppercase letters, the method should shout at the user
	 * Example: If name is PETER, then function will return: “PETER IS AMAZING”
	 * 
	 /
	@Test
	void testPeterIsShouting() {
		TribalScreaming ts = new TribalScreaming();
		String result = ts.scream("PETER");
		assertEquals("PETER IS AMAZING", result);
	}
	
	/* R4: REFACTOR - function have better quality and supports previous TCs - TC PASS
	 * 
	 * Modify your function to accept 2 people.
	 * The people are input as an array of Strings.
	 * Example: 
	 * 		INPUT: [“Peter”, “Jigesha”] 
	 * 		OUTPUT: Peter and Jigesha are amazing
	 */
	@Test
	void testTwoPeopleAreAmazing() {
		TribalScreaming ts = new TribalScreaming();
		String[] array = {"Peter", "Jigesha"};
		String result = ts.scream(array);
		assertEquals("Peter and Jigesha are amazing", result);

		String[] array2 = {"Albert", "Pritesh"};
		result = ts.scream(array2);
		assertEquals("Albert and Pritesh are amazing", result);
		
		String[] array3 = {"Peter"};
		result = ts.scream(array3);
		assertEquals("Peter is amazing", result);
		
	}
	
	/* R5: REFACTOR - function optimized - TC PASS
	 * 
	 * Modify the function to handle more than 2 people.
	 * In your final output, separate each person’s name using a “comma”. 
	 * The last person should have an “and” before the name.
	 * Example: 
	 * 		INPUT: [“Peter”, “Jigesha”, “Marcos”] 
	 * 		OUTPUT: Peter, Jigesha, and Marcos are amazing
	 */
	@Test
	void testMoreThan2PeopleAreAmazing() {
		TribalScreaming ts = new TribalScreaming();
		String[] array = {"Peter", "Jigesha", "Marcos"};
		String result = ts.scream(array);
		assertEquals("Peter, Jigesha and Marcos are amazing", result);
		//System.out.println(result);
		
		String[] array1 = {"Peter", "Jigesha", "Marcos", "Pritesh"};
		result = ts.scream(array1);
		assertEquals("Peter, Jigesha, Marcos and Pritesh are amazing", result);
		//System.out.println(result);
		
	}
	
	/* R6: REFACTOR - function was optimized and now all the others worked - TC PASS
	 * 
	 * Extend your function to handle a mixture (jumble) of uppercase and regular names.
	 * Assume there will only be 1 name that is uppercase.
	 * Example: 
	 * 		INPUT: [“Peter”, “JIGESHA”] 
	 * 		OUTPUT: Peter is amazing. JIGESHA ALSO!
	 * 
	 * 		INPUT: [“Peter”, “JIGESHA”, “Marcos”]
	 * 		OUTPUT: Peter and Marcos are amazing. JIGESHA ALSO!
	 * 
	 *  	INPUT: [“JIGESHA”, “Peter”, “Marcos”, “Albert”]
	 * 		OUTPUT: Peter, Marcos and Albert are amazing. JIGESHA ALSO!
	 */
	@Test
	void testShoutingAtALotOfPeople() {
		TribalScreaming ts = new TribalScreaming();
		String[] array = {"Peter", "JIGESHA"};
		String result = ts.scream(array);
		assertEquals("Peter is amazing. JIGESHA ALSO!", result);
		//System.out.println(result);
		
		String[] array2 = {"Peter", "JIGESHA", "Marcos"};
		result = ts.scream(array2);
		assertEquals("Peter and Marcos are amazing. JIGESHA ALSO!", result);
		//System.out.println(result);
		
		String[] array3 = {"JIGESHA", "Peter", "Marcos", "Albert"};
		result = ts.scream(array3);
		assertEquals("Peter, Marcos and Albert are amazing. JIGESHA ALSO!", result);
		//System.out.println(result);
		
		
	}
}
